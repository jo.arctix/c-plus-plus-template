#!/bin/bash

set -xe

readonly WORKSPACE_DIR=$(pwd)
declare -r BUILD_TYPE="${1:-debug}" # "release"
declare -r BUILD_DIR="${WORKSPACE_DIR}/cmake-build-${BUILD_TYPE}"

# Configure CMake
cmake --log-level=TRACE -B "${BUILD_DIR}" -DCMAKE_BUILD_TYPE="${BUILD_TYPE^}"

# Build
cmake --build "${BUILD_DIR}" --config "${BUILD_TYPE^}"

# Test
cd "${BUILD_DIR}"
ctest -C "${BUILD_TYPE^}"
cd ..

exit $?

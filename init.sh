rm -rf .git
git init

rm -rf README.md
touch README.md
git add README.md

rm -rf .gitignore
curl https://www.toptal.com/developers/gitignore/api/c,c++,linux,windows,macos,vim,cmake,clion,emacs,visualstudio,visualstudiocode,git --output .gitignore
git add .gitignore

rm -rf .gitmodules
touch .gitmodules
git add .gitmodules

cd external
rm -rf fmt
git submodule add --depth 1 -- https://github.com/fmtlib/fmt.git
rm -rf spdlog
git submodule add --depth 1 -- https://github.com/gabime/spdlog.git
rm -rf doctest
git submodule add --depth 1 -- https://github.com/onqtam/doctest.git
cd ..

git commit -m "initial commit"
